package com.streamsphere.streamsphere.GraphQlModels;

import lombok.Data;

@Data
public class UserInput {

    private String userName;

    private String email;

    private String password;

}
