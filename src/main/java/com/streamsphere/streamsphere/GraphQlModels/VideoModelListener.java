package com.streamsphere.streamsphere.GraphQlModels;

import org.springframework.stereotype.Component;

import com.streamsphere.streamsphere.Models.VideoModel;

import jakarta.persistence.PostPersist;
import jakarta.persistence.PostUpdate;
import reactor.core.publisher.Sinks;

@Component
public class VideoModelListener {
    private static Sinks.Many<VideoModel> sink;

    public static void setSink(Sinks.Many<VideoModel> sink) {
        VideoModelListener.sink = sink;
    }

    @PostUpdate
    @PostPersist
    public void onPostUpdateOrPersist(VideoModel videoModel) {
        if (sink != null) {
            sink.tryEmitNext(videoModel);
        }
    }
}
