package com.streamsphere.streamsphere.VideosControl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SubscriptionMapping;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.streamsphere.streamsphere.GraphQlModels.VideoModelListener;
import com.streamsphere.streamsphere.Models.VideoModel;
import com.streamsphere.streamsphere.VideosControl.VideoDetails.FetchAllVideos;
import com.streamsphere.streamsphere.VideosControl.VideoDetails.FetchUserVideos;
import com.streamsphere.streamsphere.VideosControl.VideoDetails.FetchVideoState;
import com.streamsphere.streamsphere.VideosControl.VideoOps.UpdateVideoState;

import jakarta.annotation.PostConstruct;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

@Controller
public class VideoController {

    @Autowired
    private FetchVideoState fetchVideoState;

    @Autowired
    private FetchAllVideos fetchAllVideos;

    @Autowired
    private FetchUserVideos fetchUserVideos;

    @Autowired
    private UpdateVideoState updateVideoState;

    private final Sinks.Many<VideoModel> sink;

    public VideoController()
    {
        this.sink = Sinks.many().multicast().onBackpressureBuffer();
    }

    @PostConstruct
    public void init()
    {
        VideoModelListener.setSink(sink);
    }

    @QueryMapping("getVideoDetailsById")
    public Object getVideoDetailsById(@Argument String videoId) {
        ResponseEntity<?> reponse = fetchVideoState.fetchVideoStateService(videoId);
        return reponse.getBody();
    }

    @QueryMapping("getAllVideos")
    public List<VideoModel> getAllVideos() {
        return fetchAllVideos.fetchAllVideos();
    }

    @QueryMapping("getVideosOfUser")
    public Object getVideosOfUser(@Argument String token, @Argument String userName) {
        return fetchUserVideos.fetchUserVideosService(token, userName);
    }

    @SubscriptionMapping
    public Flux<VideoModel> videoState(@Argument UUID videoId)
    {
        return sink.asFlux().filter(video->video.getVideoId().equals(videoId));
    }

    @MutationMapping("updateVideoState")
    public Object updateVideoState(@Argument UUID videoId, @Argument String videoState)
    {
        return updateVideoState.updateVideoStateService(videoId,videoState);
    }

}
