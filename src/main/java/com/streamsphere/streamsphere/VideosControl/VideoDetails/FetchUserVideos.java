package com.streamsphere.streamsphere.VideosControl.VideoDetails;

import java.security.PrivateKey;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.streamsphere.streamsphere.AccessControl.AuthService;
import com.streamsphere.streamsphere.AccessControl.RSAPrivateKeyConverter;
import com.streamsphere.streamsphere.Models.ResponseMessage;
import com.streamsphere.streamsphere.Models.VideoModel;
import com.streamsphere.streamsphere.Repositories.UserRepository;
import com.streamsphere.streamsphere.Repositories.VideoRepository;

@Service
public class FetchUserVideos {

    @Autowired
    private VideoRepository videoRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthService authService;

    @Autowired
    private RSAPrivateKeyConverter rsaPrivateKeyConverter;

    public Object fetchUserVideosService(String token, String userName) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            String privateKey = userRepository.findByUserName(userName).getPrivateKey();
            PrivateKey rsaPrivateKey = rsaPrivateKeyConverter.convertStringToRSAPrivateKey(privateKey);
            String email = authService.verifyToken(token, rsaPrivateKey).getBody().getMessage();

            List<VideoModel> videos = videoRepository.findByUserId(userRepository.findByEmail(email).getUserId());

            if (videos != null && !videos.isEmpty()) {
                return videos;
            } else {
                return Collections.emptyList();
            }

        } catch (Exception e) {
            responseMessage.setSuccess(false);
            responseMessage.setMessage("Internal Server Error. Reason: " + e.getMessage());
            return ResponseEntity.status(500).body(responseMessage);
        }

    }

}
