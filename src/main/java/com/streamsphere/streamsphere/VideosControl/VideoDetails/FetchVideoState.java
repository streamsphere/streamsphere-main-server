package com.streamsphere.streamsphere.VideosControl.VideoDetails;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.streamsphere.streamsphere.Models.ResponseMessage;
import com.streamsphere.streamsphere.Models.VideoModel;
import com.streamsphere.streamsphere.Repositories.VideoRepository;

@Service
public class FetchVideoState {

    @Autowired
    private VideoRepository videoRepository;

    public ResponseEntity<?> fetchVideoStateService(String videoId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            VideoModel videoFromDB = videoRepository.findByVideoId(UUID.fromString(videoId));
            if (videoFromDB != null) {
                return ResponseEntity.status(200).body(videoFromDB);
            } else {
                responseMessage.setSuccess(false);
                responseMessage.setMessage("Video not found with this ID");
                return ResponseEntity.status(404).body(responseMessage);
            }
        } catch (Exception e) {
            responseMessage.setSuccess(false);
            responseMessage.setMessage("Internal Server Error. Reason: " + e.getMessage());
            return ResponseEntity.status(500).body(responseMessage);
        }
    }
}
