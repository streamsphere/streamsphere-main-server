package com.streamsphere.streamsphere.VideosControl.VideoDetails;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.streamsphere.streamsphere.Models.VideoModel;
import com.streamsphere.streamsphere.Repositories.VideoRepository;

@Service
public class FetchAllVideos {

    @Autowired
    private VideoRepository videoRepository;

    public List<VideoModel> fetchAllVideos()
    {
        List<VideoModel> videos = videoRepository.findAll();
        return videos;        
    }
    
}
