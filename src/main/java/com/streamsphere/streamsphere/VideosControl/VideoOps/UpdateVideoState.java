package com.streamsphere.streamsphere.VideosControl.VideoOps;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.streamsphere.streamsphere.Models.ResponseMessage;
import com.streamsphere.streamsphere.Models.VideoModel;
import com.streamsphere.streamsphere.Repositories.VideoRepository;

@Service
public class UpdateVideoState {

    @Autowired
    private VideoRepository videoRepository;

    public Object updateVideoStateService(UUID videoId, String state) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            VideoModel videoFromDB = videoRepository.findByVideoId(videoId);
            if (videoFromDB != null) {
                videoFromDB.setState(state);
                videoRepository.save(videoFromDB);

                responseMessage.setSuccess(true);
                responseMessage.setMessage("Video state updated");
                return ResponseEntity.status(200).body(responseMessage);
            } else {
                responseMessage.setSuccess(false);
                responseMessage.setMessage("Video not found");
                return ResponseEntity.status(400).body(responseMessage);
            }

        } catch (Exception e) {
            responseMessage.setSuccess(false);
            responseMessage.setMessage(
                    "Internal Server Error.(UpdateVideoState.updateVideoStateService) Reason: " + e.getMessage());
            return ResponseEntity.status(500).body(responseMessage);
        }
    }

}
