package com.streamsphere.streamsphere.Repositories;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.streamsphere.streamsphere.Models.UserModel;

public interface UserRepository extends JpaRepository<UserModel, UUID> {

    UserModel findByUserName(String userName);
    UserModel findByEmail(String email);
    UserModel findByUserId(UUID userId);
}
