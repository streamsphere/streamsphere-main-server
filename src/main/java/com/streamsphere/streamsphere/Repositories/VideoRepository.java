package com.streamsphere.streamsphere.Repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.streamsphere.streamsphere.Models.VideoModel;
import java.util.List;



public interface VideoRepository extends JpaRepository<VideoModel,UUID>{
    VideoModel findByVideoId(UUID videoId);
    List<VideoModel> findByUserId(UUID userId);
}
