package com.streamsphere.streamsphere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamsphereApplication {

	public static void main(String[] args) {
		SpringApplication.run(StreamsphereApplication.class, args);
	}

}
