package com.streamsphere.streamsphere.AccessControl;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.streamsphere.streamsphere.Models.ResponseMessage;
import com.streamsphere.streamsphere.Models.TokenAndPrivateKey;

@Service
public class AuthService {

    private KeyPair generateRSAKeyPair() throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048); // Key size of 2048 bits is recommended
        return keyPairGenerator.generateKeyPair();
    }

    public TokenAndPrivateKey generateToken(String email)
            throws Exception {

        KeyPair keyPair = generateRSAKeyPair();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();

        JWSSigner signer = new RSASSASigner(privateKey);

        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject(email)
                .build();

        SignedJWT signedJWT = new SignedJWT(new JWSHeader.Builder(JWSAlgorithm.RS256).build(), claimsSet);
        signedJWT.sign(signer);

        JWEObject jweObject = new JWEObject(new JWEHeader.Builder(JWEAlgorithm.RSA_OAEP_256, EncryptionMethod.A256GCM)
                .contentType("JWT")
                .build(), new Payload(signedJWT));

        jweObject.encrypt(new RSAEncrypter(publicKey));
        
        TokenAndPrivateKey tokenAndPrivateKey = new TokenAndPrivateKey();
        tokenAndPrivateKey.setToken(jweObject.serialize());
        tokenAndPrivateKey.setPrivateKey(Base64.getEncoder().encodeToString(privateKey.getEncoded()));

        return tokenAndPrivateKey;
    }

    public ResponseEntity<ResponseMessage> verifyToken(String token, PrivateKey privateKey) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {

            // Parse and decrypt the JWE token
            JWEObject jweObject = JWEObject.parse(token);

            // Decrypt using the private key
            RSADecrypter decrypter = new RSADecrypter(privateKey);
            jweObject.decrypt(decrypter);

            // Extract and return the subject from the claims
            SignedJWT signedJWT = jweObject.getPayload().toSignedJWT();
            if (signedJWT == null) {
                throw new JOSEException("Invalid JWT token structure");
            }

            JWTClaimsSet claimsSet = signedJWT.getJWTClaimsSet();
            responseMessage.setSuccess(true);
            responseMessage.setMessage( claimsSet.getSubject());
            return ResponseEntity.status(200).body(responseMessage);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
            responseMessage.setMessage("Internal Server Error.(AuthService.verifyToken) Reason: " + e.getMessage());
            return ResponseEntity.status(500).body(responseMessage);
        }
    }

}
