package com.streamsphere.streamsphere.AccessControl;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.streamsphere.streamsphere.Models.ResponseMessage;
import com.streamsphere.streamsphere.Models.TokenAndPrivateKey;
import com.streamsphere.streamsphere.Models.UserModel;
import com.streamsphere.streamsphere.Repositories.UserRepository;

@Service
public class Login {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthService authService;

    public ResponseEntity<ResponseMessage> loginService(String email, String passwordString) {
        ResponseMessage responseMessage = new ResponseMessage();

        try {
            UserModel userExists = userRepository.findByEmail(email);
            if (userExists != null) {
                if (BCrypt.checkpw(passwordString, userExists.getPassword())) {
                    TokenAndPrivateKey tokenAndPrivateKey = authService.generateToken(userExists.getEmail());
                    userExists.setPrivateKey(tokenAndPrivateKey.getPrivateKey());
                    userRepository.save(userExists);
                    responseMessage.setSuccess(true);
                    responseMessage
                            .setMessage(tokenAndPrivateKey.getToken() + "," + userExists.getUserName());

                    return ResponseEntity.status(200).body(responseMessage);
                } else {
                    responseMessage.setSuccess(false);
                    responseMessage.setMessage("Invalid email or password");
                    return ResponseEntity.status(200).body(responseMessage);
                }
            } else {
                responseMessage.setSuccess(false);
                responseMessage.setMessage("User not found");
                return ResponseEntity.status(200).body(responseMessage);
            }
        } catch (Exception e) {
            responseMessage.setSuccess(false);
            responseMessage.setMessage("Internal Server Error. Reason: " + e.getMessage());
            return ResponseEntity.status(500).body(responseMessage);
        }
    }
}
