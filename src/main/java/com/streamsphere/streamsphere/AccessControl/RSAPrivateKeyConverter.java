package com.streamsphere.streamsphere.AccessControl;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

import org.springframework.stereotype.Service;

@Service
public class RSAPrivateKeyConverter {

    public PrivateKey convertStringToRSAPrivateKey(String privateKeyStr) throws Exception {
        byte[] decodedPrivateKeyBytes = Base64.getDecoder().decode(privateKeyStr);
        
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decodedPrivateKeyBytes);
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        
        if (privateKey instanceof RSAPrivateKey) {
            return privateKey;
        } else {
            throw new IllegalArgumentException("Private key is not an RSA private key");
        }
    }
}
