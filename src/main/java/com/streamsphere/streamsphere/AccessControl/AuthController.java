package com.streamsphere.streamsphere.AccessControl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.streamsphere.streamsphere.GraphQlModels.UserInput;
import com.streamsphere.streamsphere.Models.ResponseMessage;
import com.streamsphere.streamsphere.Models.UserModel;

@Controller
public class AuthController {

    @Autowired
    private UserRegistration userRegistration;

    @Autowired
    private Login login;

    @Autowired
    private FetchUserDetails fetchUserDetails;

    @MutationMapping("registerUser")
    public ResponseMessage registerUser(@Argument("userInput") UserInput userInput) {

        UserModel user = new UserModel();
        user.setUserName(userInput.getUserName());
        user.setEmail(userInput.getEmail());
        user.setPassword(userInput.getPassword());

        ResponseEntity<ResponseMessage> responseMessage = userRegistration.registerUserService(user);
        return responseMessage.getBody();
    }

    @QueryMapping("getUserDetailsByToken")
    public Object getUserDetailsByToken(@Argument String token, @Argument String userName) {
        ResponseEntity<?> responseMessage = fetchUserDetails.fetchUserDetailsByTokenService(token, userName);
        return responseMessage.getBody();
    }

    @QueryMapping("login")
    public Object login(@Argument String email ,@Argument String password)
    {
        ResponseEntity<ResponseMessage> response = login.loginService(email, password);
        return response.getBody();
    }

    @QueryMapping("getUserDetailsByUserId")
    public Object getUserDetailsByUserId(@Argument String userId)
    {
        ResponseEntity<?> response = fetchUserDetails.fetchUserDetailsByUserId(UUID.fromString(userId));
        return response.getBody();
    }

}
