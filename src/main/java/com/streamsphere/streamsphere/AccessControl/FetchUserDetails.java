package com.streamsphere.streamsphere.AccessControl;

import java.security.PrivateKey;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.streamsphere.streamsphere.Models.ResponseMessage;
import com.streamsphere.streamsphere.Models.UserModel;
import com.streamsphere.streamsphere.Repositories.UserRepository;

@Service
public class FetchUserDetails {

    @Autowired
    private AuthService authService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RSAPrivateKeyConverter rsaPrivateKeyConverter;

    public ResponseEntity<?> fetchUserDetailsByTokenService(String token, String userName) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            UserModel user = userRepository.findByUserName(userName);
            if (user != null) {
                String privateKey = user.getPrivateKey();
                PrivateKey rsaPrivateKey = rsaPrivateKeyConverter.convertStringToRSAPrivateKey(privateKey);

                String email = authService.verifyToken(token, rsaPrivateKey).getBody().getMessage();
                if (email.equals(user.getEmail())) {
                    return ResponseEntity.status(200).body(user);
                } else {
                    responseMessage.setSuccess(false);
                    responseMessage.setMessage("Invalid token");
                    return ResponseEntity.status(400).body(responseMessage);
                }
            } else {
                responseMessage.setSuccess(false);
                responseMessage.setMessage("User not found");
                return ResponseEntity.status(404).body(responseMessage);
            }

        } catch (Exception e) {
            responseMessage.setSuccess(false);
            responseMessage
                    .setMessage("Internal Server Error.(FetchUserDetails.fetchUserDetailsByTokenService) Reason: "
                            + e.getMessage());
            return ResponseEntity.status(500).body(responseMessage);
        }
    }

    public ResponseEntity<?> fetchUserDetailsByUserId(UUID userId) {
        ResponseMessage responseMessage = new ResponseMessage();
        try {
            UserModel userDetails = userRepository.findByUserId(userId);
            if (userDetails == null) {
                responseMessage.setSuccess(false);
                responseMessage.setMessage("User not found");
                return ResponseEntity.status(404).body(responseMessage);
            } else {
                return ResponseEntity.status(200).body(userDetails);
            }
        } catch (Exception e) {
            responseMessage.setSuccess(false);
            responseMessage.setMessage(
                    "Internal Server Error.(FetchUserDetails.fetchUserDetailsByUserId) Reason: " + e.getMessage());
            return ResponseEntity.status(500).body(responseMessage);
        }
    }
}
