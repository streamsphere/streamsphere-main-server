package com.streamsphere.streamsphere.Models;

import java.time.LocalDateTime;
import java.util.UUID;

import com.streamsphere.streamsphere.GraphQlModels.VideoModelListener;

import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name="videos_db")
@EntityListeners(VideoModelListener.class)
public class VideoModel {

    @Id
    private UUID videoId;

    private UUID userId;

    private String videoURL;

    private LocalDateTime createdAt;

    private String title;
    
    private String description;

    private String state;

    private String thumbNailURL;
    
}
