package com.streamsphere.streamsphere.Models;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class TokenAndPrivateKey {
    private String privateKey;
    private String token;
}
