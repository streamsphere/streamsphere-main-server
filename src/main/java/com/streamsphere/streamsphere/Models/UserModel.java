package com.streamsphere.streamsphere.Models;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "user_db")
public class UserModel {

    @Id
    @GeneratedValue(generator = "UUID")
    private UUID userId;

    @Column(unique = true)
    private String userName;
    
    private String channelName;

    @Column(unique = true)
    private String email;

    private String password;

    private String avatarURL;

    @Column(length = 2500)
    private String privateKey;

}
